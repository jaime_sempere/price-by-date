# Why this documentation

I watched this presentation some time ago, and definitely I was influenced by it.

<iframe width="640" height="360" src="https://www.youtube.com/embed/ftnVllssoI8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since then, in all the projects that I have worked, I have missed (a lot) some 
_developers notes/documentation_, as the guy in the video refers.

Of course, confluence is always there, but let's be honest, confluence is far from be a good 
solution (slow, over-bloated, and the documentation is always left for the end).

Mkdocs, in the other hand, feels natural, powerful and comfortable to use it while 
you are coding.

In any new project, I would try to recommend the team a `Building docs like code` solution

> MkDocs is like a Readme.md with steroids