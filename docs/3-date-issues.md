# Date issues

## Be careful of dates

<img src="../imgs/abe.gif"/>

> If you ever travel back in time, stay away from any Date issues.   
> Even the tiniest change can alter the things
> in ways you can't imagine   
> -- <cite>Abraham Simpson</cite>

Jokes aside, dates are always tricky to handle.

One of my fist approaches was to use the controller method signature
to deal with the date parameter and format.

<br>

For example:

__Detail of date parameter on the _PriceByDateController.java_:__

``` java
      @ApiParam(value = "Time and Date where price applies. Format: yyyy-MM-dd-HH.mm.ss, ej: 2020-06-14-00.00.00")
      @RequestParam("date") 
      @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm.ss") 
      LocalDateTime date) {
```

<br>

This worked perfectly, I was able to use the endpoint, passed the date
parameter with the requested format and worked like a charm:

![](./imgs/endpoint-response.png)


### ...But swagger was broken

![](./imgs/swagger-date-issue.png)


The specific format for the date used, has a big downside:
I was not able to use it swagger with this date format, the input
was red colored and execute button was disabled.

According to Swagger specification, it seems that Open API
only accepts `ISO 8601` format:   
[link 1](https://swagger.io/docs/specification/data-models/data-types/)  
[link 2](https://datatracker.ietf.org/doc/html/rfc3339#section-5.6)

### Now what?

At this point, I have two possibilities:

- Stick to `ISO 8601` format
- Use string on the controller, and manage the format by myself

I decided to choose the latter option: the exercise was written
with another date format (i.e. `yyyy-MM-dd-HH.mm.ss`), and it
seems more natural and right to respect that format for the
endpoint (and in fact, I already have the tests specified with
this format)

The downside is that we will need a little more of logic, I decided
to isolate on a utility class.
In a bigger project, this utility class could be part of a
common library used by all the microservices.

``` java
public class DateParser {

  public static LocalDateTime parseToDateTime(String dateString) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");
    return LocalDateTime.parse(dateString, formatter);
  }
}
```

Anyway, I crossed my fingers, and I hoped not altering the future too much...

