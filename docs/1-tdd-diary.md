# Tests and TDD 

I have used a meticulous TDD approach for developing the whole solution

## Test pyramid

I wrote 3 types of tests using different tools that Spring Boot supplies:

1. Unit Tests: they are found on the same package as the classes under test, but on the
`test` package instead of `src`. 
   I used Junit5 for these.
2. Integration Tests:   
   I wrote one test for testing swagger web was running properly (`SwaggerUILoadsTest.java`)  
   I used a `@WebMvcTest` instead of a `@SpringBootTest` because is faster as it does not load the
   entire context, just the controller part.  
   This would help as reference for future developers to find the swagger url.  
   I wrote another to test the repository (actually the query for the exercise), 
   loading the whole context with `@SpringBootTest`
3. Acceptance Tests:  
   Finally I used the `io.restassured.RestAssured` library for write the acceptance test, 
   testing directly the endpoint with inputs and expected outputs
   