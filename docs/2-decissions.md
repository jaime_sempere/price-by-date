# Decisions taken

In the development of the solution, a lot of questions popped up.

I tried to take the more `common sense` solutions whenever I faced one.

This is a list of some decisions taken in the process of building the project.

## Domain/entity types

- brandId:  as a company can have a lot of brands, but the number will not be huge, I
  decided `Integer` was enough for this field
- productId: I really think `Integer` will be more than enough for this field, but just in case,
  finally I decided to be conservative, and I defined as Long

## Returning the price

On the endpoint, I was requested to return the price, but the specification was not very
fine-grained detailed:

For a price of 32.5, what should we return?:

- 32.5
- 32.50
- '32.50'
- '35.50 €'
- '32.50 EUR'

Finally, I decided to leave the API _frontend consumption ready_ and I decided to return
`35.50 €`. I even thought to return two fields:

    "price":32.5, "textPrice": "32.50 €"

but I tried to stay away of overengineer any solution. As the requirements were not precise about
this point, and CURRENCY was supplied, I decided the `"32.50 €"` was the most wanted solution.
  
## Updated

Finally, as explained on first page, the `,` was used as separator for decimal part, instead of
the `.`, due to Pull&Bear online shop was using this format.