# JPA DTO projection

Instead of mapping the query directly to the entity, I have used DTO projection for
query the result (`PriceQueryProjectionDTO.java` class).

## Why?

You can read more about why to use DTO projections instead of Entity projections
[here](https://thorben-janssen.com/spring-data-jpa-query-projections/#Dynamic_Projections)

> Spring Data JPA supports all three projections defined by the JPA specification. Entities are the best fit for write operations. Moreover, you should use class-based DTO projections for read operations.

Or in this book:
[Spring Boot Persistence Best Practices](https://www.apress.com/gp/book/9781484256251)

## Code example

``` java
public interface PriceRepository extends PagingAndSortingRepository<Price, Long> {

  @Query(
      value =
          "SELECT new com.jaime.pricebydate.dto.PriceQueryProjectionDto(p.brandId, p.productId, p.priceList, p.price, p.startDate, p.endDate, p.currency) FROM Price p "
              + "WHERE p.brandId = :brandId AND p.productId = :productId AND :date BETWEEN p.startDate AND p.endDate "
              + "ORDER BY p.priority DESC")
  List<PriceQueryProjectionDto> findByBrandIdProductIdAndDate(
      @Param("brandId") Integer brandId,
      @Param("productId") Long productId,
      @Param("date") LocalDateTime date,
      Pageable pageable);
}
```

The syntax of the query is not the less verbose of the world, but with the IDE helps it is not
so hard to write it. 

Using `Interface projection`, produces a better syntax for the query (and you can even use native 
queries), but their performance is slower and, most important, testing is more difficult.

## Really... why?

For this exercise, it would not have been an issue to map the query directly to the Entity.

!!! info "Everybody knows how to map a query to an Entity"
    As that's something everybody knows, I wanted to prove that I can handle more advanced 
    strategies like this one (although a simple solution, if does not have any downside, 
    will always be better)

In this way, the query is un-coupled from the entity (which is good, because in the future, 
the entity can grow, have other entities attached, and trigger
[n+1 query issues](https://vladmihalcea.com/n-plus-1-query-problem/)... etc)

I have seen how a simple query, triggered thousands of n+1 queries due to
the use of Entity projection.

## Technical debt

As a downside of DTO projection, you will need to have an extra DTO.

We can live with that, and on the other hand, it gives you some advantages (for example, mapping
this extra DTO to the rest-exposed DTO tends to be easier than Entity-DTO mapping)

Plus, this DTO is lighter than the entity.

Another downside of using JPQL and this DTO projection, is that, JPQL does not allow you to use the 
`LIMIT` keyword on the query, that's why we have needed `Pageable` for add the limit clause.
In this way, Hibernate will compose the resultant query with this clause.
