# Other points

- I have added the `intellij-java-google-style.xml` used to format 
  the code to the repository. In this way any future developer
  can keep writing the code respecting the format and style
  