# Price per date

This application return the proper price depending on the time requested.

In other words, a product has different offers/prices for different dates. Therefore, depending on 
the date, one price or other should be returned

## Documentation

You can find the generated documentation on the following link:
https://jaime_sempere.gitlab.io/price-by-date/

Developers can find (and extend) the written documentation on `docs` folder of 
the project

The documentation is developed using [mkdocs](http://www.mkdocs.org)

### Two extra files 

There are two extra files on the root path.

You will find a `.gitlab-ci.yml`, that will trigger the CI 
for generating the documentation on every push.
The `mkdocs.yml` file is used by mkdocs to serve the documentation
site as you code.
