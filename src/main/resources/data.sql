
SET @dateFormat = 'YYYY-MM-DD-HH24-MI-SS';

SET @startDate1 =  TO_TIMESTAMP('2020-06-14-00.00.00', @dateFormat);
SET @endDate1 =  TO_TIMESTAMP('2020-12-31-23.59.59', @dateFormat);

SET @startDate2 =  TO_TIMESTAMP('2020-06-14-15.00.00', @dateFormat);
SET @endDate2 =  TO_TIMESTAMP('2020-06-14-18.30.00', @dateFormat);

SET @startDate3 =  TO_TIMESTAMP('2020-06-15-00.00.00', @dateFormat);
SET @endDate3 =  TO_TIMESTAMP('2020-06-15-11.00.00', @dateFormat);

SET @startDate4 =  TO_TIMESTAMP('2020-06-15-16.00.00', @dateFormat);
SET @endDate4 =  TO_TIMESTAMP('2020-12-31-23.59.59', @dateFormat);

insert into prices
( price_id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr )
values
( 1, 1, @startDate1, @endDate1, 1, 35455, 0, 35.50, 'EUR');
insert into prices
( price_id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr )
values
( 2, 1, @startDate2, @endDate2, 2, 35455, 1, 25.45, 'EUR');
insert into prices
( price_id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr )
values
( 3, 1, @startDate3, @endDate3, 3, 35455, 1, 30.50, 'EUR');
insert into prices
( price_id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr )
values
( 4, 1, @startDate4, @endDate4, 4, 35455, 1, 38.95, 'EUR');
