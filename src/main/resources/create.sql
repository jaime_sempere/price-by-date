create sequence hibernate_sequence start with 1 increment by 1
create table price
(
    price_id   bigint         not null,
    brand_id   integer        not null,
    curr       varchar(255)   not null,
    end_date   timestamp      not null,
    price      decimal(19, 2) not null,
    price_list integer        not null,
    priority   integer        not null,
    product_id bigint         not null,
    start_date timestamp      not null,
    primary key (price_id)
)
create sequence hibernate_sequence start with 1 increment by 1
