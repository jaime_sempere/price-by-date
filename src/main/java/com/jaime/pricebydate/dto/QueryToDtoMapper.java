package com.jaime.pricebydate.dto;

import com.jaime.pricebydate.utils.DateParser;
import com.jaime.pricebydate.utils.PriceFormatter;

public class QueryToDtoMapper {

  public static PriceByDateResponseDto convert(PriceQueryProjectionDto priceQueryDto) {

    String readablePrice = PriceFormatter.format(priceQueryDto.getPrice(), priceQueryDto.getCurrency());
    String starDateString = DateParser.parseToString(priceQueryDto.getStartDate());
    String endDateString = DateParser.parseToString(priceQueryDto.getEndDate());

    return PriceByDateResponseDto.builder()
        .brandId(priceQueryDto.getBrandId())
        .productId(priceQueryDto.getProductId())
        .priceList(priceQueryDto.getPriceList())
        .price(readablePrice)
        .startDate(starDateString)
        .endDate(endDateString)
        .build();
  }
}
