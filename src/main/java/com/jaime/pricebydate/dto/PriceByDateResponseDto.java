package com.jaime.pricebydate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceByDateResponseDto {

  private Integer brandId;
  private Long productId;
  private Integer priceList;
  private String price;
  private String startDate;
  private String endDate;
}
