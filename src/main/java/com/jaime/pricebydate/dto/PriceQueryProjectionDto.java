package com.jaime.pricebydate.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Value;

@Value
public class PriceQueryProjectionDto {

  private Integer brandId;
  private Long productId;
  private Integer priceList;
  private BigDecimal price;
  private LocalDateTime startDate;
  private LocalDateTime endDate;
  private String currency;
}
