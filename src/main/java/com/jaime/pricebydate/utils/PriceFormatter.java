package com.jaime.pricebydate.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import org.springframework.stereotype.Component;

@Component
public class PriceFormatter {

  public static String format(BigDecimal price, String currency) {

    price = price.setScale(2, BigDecimal.ROUND_UP);

    if ("EUR".equals(currency)) {
      return formatPriceByCurrency(price, ',', " €");
    } else {
      return formatPriceByCurrency(price, '.', " $");
    }
  }

  private static String formatPriceByCurrency(
      BigDecimal price, char decimalSeparator, String currencySymbolToAppend) {

    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    symbols.setDecimalSeparator(decimalSeparator);
    DecimalFormat df = new DecimalFormat("###.00", symbols);

    return df.format(price) + currencySymbolToAppend;
  }
}
