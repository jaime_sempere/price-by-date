package com.jaime.pricebydate.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateParser {

  public static final String PATTERN = "yyyy-MM-dd-HH.mm.ss";

  public static LocalDateTime parseToDateTime(String dateString) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
    return LocalDateTime.parse(dateString, formatter);
  }

  public static String parseToString(LocalDateTime dateTime) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN);
    return dateTime.format(formatter);
  }
}
