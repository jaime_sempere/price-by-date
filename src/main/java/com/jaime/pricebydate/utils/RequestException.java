package com.jaime.pricebydate.utils;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RequestException extends ResponseStatusException {

  private RequestException(HttpStatus httpStatus, String message) {
    super(httpStatus, message);
  }

  public static RequestException INVALID_DATE_FORMAT() {
    return new RequestException(
        BAD_REQUEST,
        "Date format should have this pattern: yyyy-MM-dd-HH.mm.ss  (i.e.: 2020-06-14-00.01.00)");
  }
  public static RequestException NOT_FOUND() {
    return new RequestException(
        NOT_FOUND,
        "No price found for input parameters");
  }
}
