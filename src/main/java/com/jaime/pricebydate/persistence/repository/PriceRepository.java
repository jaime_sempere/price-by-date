package com.jaime.pricebydate.persistence.repository;

import com.jaime.pricebydate.dto.PriceQueryProjectionDto;
import com.jaime.pricebydate.persistence.entity.Price;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PriceRepository extends PagingAndSortingRepository<Price, Long> {

  @Query(
      value =
          "SELECT new com.jaime.pricebydate.dto.PriceQueryProjectionDto(p.brandId, p.productId, p.priceList, p.price, p.startDate, p.endDate, p.currency) FROM Price p "
              + "WHERE p.brandId = :brandId AND p.productId = :productId AND :date BETWEEN p.startDate AND p.endDate "
              + "ORDER BY p.priority DESC")
  List<PriceQueryProjectionDto> findByBrandIdProductIdAndDate(
      @Param("brandId") Integer brandId,
      @Param("productId") Long productId,
      @Param("date") LocalDateTime date,
      Pageable pageable);
}
