package com.jaime.pricebydate.controller;

import com.jaime.pricebydate.dto.PriceByDateResponseDto;
import com.jaime.pricebydate.service.PriceByDateServiceImpl;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("price-by-date/")
@RequiredArgsConstructor
@Slf4j
public class PriceByDateController {

  private final PriceByDateServiceImpl priceByDateService;

  @GetMapping("{brandId}/{productId}")
  @ResponseBody
  public PriceByDateResponseDto priceByDate(
    @ApiParam(value = "The brand Id where to search the product")
    @PathVariable("brandId")
    Integer brandId,

    @ApiParam(value = "The product Id to get the price")
    @PathVariable("productId")
    Long productId,

    @ApiParam( value = "Time and Date where price applies. Format: yyyy-MM-ddTHH:mm:ss, ej: 2020-06-14T00.00.00")
    @RequestParam("date")
    String date) {

    log.info( "priceByDate requested with brandId: {}, productId: {}, data: {}", brandId, productId, date);

    return priceByDateService.getPrice(brandId, productId, date);
  }
}
