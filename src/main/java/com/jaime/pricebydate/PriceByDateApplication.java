package com.jaime.pricebydate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PriceByDateApplication {

  public static void main(String[] args) {

    SpringApplication.run(PriceByDateApplication.class, args);
  }
}
