package com.jaime.pricebydate.service;

import com.jaime.pricebydate.dto.PriceByDateResponseDto;
import com.jaime.pricebydate.dto.PriceQueryProjectionDto;
import com.jaime.pricebydate.dto.QueryToDtoMapper;
import com.jaime.pricebydate.persistence.repository.PriceRepository;
import com.jaime.pricebydate.utils.DateParser;
import com.jaime.pricebydate.utils.RequestException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PriceByDateServiceImpl implements PriceByDateService {

  private final PriceRepository priceRepository;

  @Override
  public PriceByDateResponseDto getPrice(Integer brandId, Long productId, String date) {

    LocalDateTime parsedDate = validateAndParseDate(date);

    List<PriceQueryProjectionDto> pricesResultsDto =
        priceRepository.findByBrandIdProductIdAndDate(brandId, productId, parsedDate, pageLimit1());

    validateResultNotEmptyOrThrowException(pricesResultsDto);

    return QueryToDtoMapper.convert(pricesResultsDto.get(0));
  }

  private LocalDateTime validateAndParseDate(String date) {
    try {
      return DateParser.parseToDateTime(date);
    } catch (DateTimeParseException dateTimeParseException) {
      throw RequestException.INVALID_DATE_FORMAT();
    }
  }

  private void validateResultNotEmptyOrThrowException(
      List<PriceQueryProjectionDto> pricesResultDto) {
    if (pricesResultDto.isEmpty()) {
      throw RequestException.NOT_FOUND();
    }
  }

  private Pageable pageLimit1() {
    return PageRequest.of(0, 1);
  }
}
