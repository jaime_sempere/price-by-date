package com.jaime.pricebydate.service;

import com.jaime.pricebydate.dto.PriceByDateResponseDto;

public interface PriceByDateService {

  PriceByDateResponseDto getPrice(Integer brandId, Long productId, String date);

}
