package com.jaime.pricebydate.utils;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the PriceFormatterTest util class")
class PriceFormatterTest {

  @Test
  @DisplayName("Check format price for EUR currency")
  public void formatPriceEur() {
    String result = PriceFormatter.format(new BigDecimal(11123), "EUR");

    assertEquals("11123,00 €", result);
  }
  @Test
  @DisplayName("Check format price for any other different from EUR currency")
  public void formatPriceDollar() {
    String result = PriceFormatter.format(new BigDecimal(11123), "DOLLAR");

    assertEquals("11123.00 $", result);
  }

  @Test
  @DisplayName("Check format price when currency is NULL")
  public void formatPriceNull() {
    String result = PriceFormatter.format(new BigDecimal(11123), null);

    assertEquals("11123.00 $", result);
  }

}