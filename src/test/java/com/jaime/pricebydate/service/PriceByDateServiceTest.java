package com.jaime.pricebydate.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import com.jaime.pricebydate.dto.PriceByDateResponseDto;
import com.jaime.pricebydate.dto.PriceQueryProjectionDto;
import com.jaime.pricebydate.persistence.repository.PriceRepository;
import com.jaime.pricebydate.utils.DateParser;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the PriceByDateService layer")
class PriceByDateServiceTest {

  public final String REQUEST_DATE_STRING = "2020-06-14-00.01.00";
  public final int BRAND_ID = 1;
  public final long PRODUCT_ID = 1L;
  private final String CURR = "EUR";
  private final String READABLE_PRICE = "35,50 €";
  private final BigDecimal PRICE = new BigDecimal(35.50);
  private final int PRICE_LIST = 1;
  private final String START_DATE = "2020-12-31-23.59.59";
  private final String END_DATE = "2020-12-31-23.59.59";

  @Mock PriceRepository priceRepository;

  @InjectMocks PriceByDateServiceImpl priceByDateService;

  @Test
  @DisplayName("PriceByDateService.getPrice service method test")
  public void getPriceTest() {

    // given
    LocalDateTime requestDate = DateParser.parseToDateTime(REQUEST_DATE_STRING);
    LocalDateTime startDate = DateParser.parseToDateTime(START_DATE);
    LocalDateTime endDate = DateParser.parseToDateTime(END_DATE);

    given(priceRepository.findByBrandIdProductIdAndDate(BRAND_ID, PRODUCT_ID, requestDate, PageRequest.of(0,1)))
        .willReturn(
            Arrays.asList(new PriceQueryProjectionDto(
                BRAND_ID, PRODUCT_ID, PRICE_LIST, PRICE, startDate, endDate, CURR)));

    // when
    PriceByDateResponseDto priceByDateResponseDto =
        priceByDateService.getPrice(BRAND_ID, PRODUCT_ID, REQUEST_DATE_STRING);

    // then
    assertEquals(BRAND_ID, priceByDateResponseDto.getBrandId());
    assertEquals(PRODUCT_ID, priceByDateResponseDto.getProductId());
    assertEquals(PRICE_LIST, priceByDateResponseDto.getPriceList());
    assertEquals(READABLE_PRICE, priceByDateResponseDto.getPrice());
    assertEquals(START_DATE, priceByDateResponseDto.getStartDate());
    assertEquals(END_DATE, priceByDateResponseDto.getEndDate());
  }
}
