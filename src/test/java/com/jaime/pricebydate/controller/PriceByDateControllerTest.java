package com.jaime.pricebydate.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.jaime.pricebydate.dto.PriceByDateResponseDto;
import com.jaime.pricebydate.service.PriceByDateServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PriceByDateController.class)
@DisplayName("Testing the price-by-date endpoint")
@Slf4j
public class PriceByDateControllerTest {

  @Autowired private MockMvc mvc;

  @MockBean private PriceByDateServiceImpl priceByDateService;

  public static final int PRICE_LIST = 1;
  public static final String PRICE = "35.50 €";
  public static final String START_DATE = "2020-06-14-00.00.00";
  public static final String END_DATE = "2020-12-31-23.59.59";
  private final Integer BRAND_ID = 1;
  private final Long PRODUCT_ID = 35455L;
  private final String REQUEST_DATE = "2020-06-14-01.00.00";

  @Test
  @DisplayName("Request and response format test")
  public void testEndpointResult() throws Exception {

    // given
    PriceByDateResponseDto priceByDateResponseDto =
        PriceByDateResponseDto.builder()
            .brandId(Integer.valueOf(BRAND_ID))
            .productId(Long.valueOf(PRODUCT_ID))
            .priceList(PRICE_LIST)
            .price(PRICE)
            .startDate(START_DATE)
            .endDate(END_DATE)
            .build();

    given(priceByDateService.getPrice(anyInt(), anyLong(), anyString()))
        .willReturn(priceByDateResponseDto);

    // when
    String url = "/price-by-date/{brandId}/{productId}?date={date}";
    MockHttpServletResponse response =
        mvc.perform(get(url, BRAND_ID, PRODUCT_ID, REQUEST_DATE)).andReturn().getResponse();
    response.setCharacterEncoding("UTF-8"); // needed for MockMvc response and the € symbol

    String jsonExpected =
        "{\"brandId\":1,\"productId\":35455,\"priceList\":1,\"price\":\"35.50 €\","
            + "\"startDate\":\"2020-06-14-00.00.00\",\"endDate\":\"2020-12-31-23.59.59\"}";

    // then
    assertEquals(HttpStatus.OK.value(), response.getStatus());
    assertEquals(jsonExpected, response.getContentAsString());
  }
}
