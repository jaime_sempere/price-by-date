package com.jaime.pricebydate.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.jaime.pricebydate.utils.DateParser;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing the QueryToDtoMapper")
class QueryToDtoMapperTest {

  private final BigDecimal PRICE = new BigDecimal(35.50);
  private final String CURR = "EUR";
  private final String READABLE_PRICE = "35,50 €";
  private final int PRICE_LIST = 1;
  private final Integer BRAND_ID = 1;
  private final Long PRODUCT_ID = 35455L;
  private final String START_DATE = "2020-06-14-00.00.00";
  private final String END_DATE = "2020-12-31-23.59.59";

  @Test
  @DisplayName("QueryToDtoMapper.convert() method test")
  public void convertTest() {

    // given
    LocalDateTime startDate = DateParser.parseToDateTime(START_DATE);
    LocalDateTime endDate = DateParser.parseToDateTime(END_DATE);
    PriceQueryProjectionDto priceQueryDto =
        new PriceQueryProjectionDto(
            BRAND_ID, PRODUCT_ID, PRICE_LIST, PRICE, startDate, endDate, CURR);

    // when
    PriceByDateResponseDto priceByDateResponseDto = QueryToDtoMapper.convert(priceQueryDto);

    // then
    assertEquals(BRAND_ID, priceByDateResponseDto.getBrandId());
    assertEquals(PRODUCT_ID, priceByDateResponseDto.getProductId());
    assertEquals(PRICE_LIST, priceByDateResponseDto.getPriceList());
    assertEquals(READABLE_PRICE, priceByDateResponseDto.getPrice());
    assertEquals(START_DATE, priceByDateResponseDto.getStartDate());
    assertEquals(END_DATE, priceByDateResponseDto.getEndDate());
  }
}
