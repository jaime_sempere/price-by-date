package com.jaime.pricebydate.acceptance;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("Acceptance tests")
@Slf4j
public class PriceByDateRestTest {

  @LocalServerPort private int port;
  private String uri;

  @PostConstruct
  public void init() {
    uri = "http://localhost:" + port;
  }

  @ParameterizedTest
  @DisplayName("Test the specified happy path cases")
  @CsvFileSource(resources = "/pricesInputOutput.csv", numLinesToSkip = 1)
  public void priceByDateEndpointTest(String brandId, String productId, String date,
      Integer outputBrandId, Integer outputProductId, Integer outputPriceList, String outputStartDate, String outputEndDate, String outputPrice
      ) {
    get(uri + "/price-by-date/" + brandId + "/" +productId + "?date="+ date)
        .then()
        .log()
        .body()
        .assertThat()
        .body("brandId", equalTo(outputBrandId))
        .body("productId", equalTo(outputProductId))
        .body("priceList", equalTo(outputPriceList))
        .body("startDate", equalTo(outputStartDate))
        .body("endDate", equalTo(outputEndDate))
        .body("price", equalTo(outputPrice))
        .statusCode(200);
  }

  @Test
  @DisplayName("When a request does not have any result, 404 is returned")
  public void priceByDatePriceDoesNotExist() {
    get(uri + "/price-by-date/1/35456?date=2020-06-14-00.01.00")
        .then()
        .log()
        .body()
        .assertThat()
        .body("message", equalTo("No price found for input parameters"))
        .statusCode(404);
  }

  @Test
  @DisplayName("When a request has the date with a no valid format, 400 is returned")
  public void priceByDateFormatException() {
    get(uri + "/price-by-date/1/1?date=2020-06-14T00.01.00")
        .then()
        .log()
        .body()
        .assertThat()
        .statusCode(400);
  }
}
