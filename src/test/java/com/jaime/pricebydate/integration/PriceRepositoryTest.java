package com.jaime.pricebydate.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.jaime.pricebydate.dto.PriceQueryProjectionDto;
import com.jaime.pricebydate.persistence.repository.PriceRepository;
import com.jaime.pricebydate.utils.DateParser;
import java.time.LocalDateTime;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest()
@AutoConfigureMockMvc
@DisplayName("Testing the Price repository")
@Slf4j
public class PriceRepositoryTest {

  @Autowired
  PriceRepository priceRepository;

  @Test
  @DisplayName("Testing query works as expected")
  public void queryTest() {

    final String requestDate = "2020-06-14-00.01.00";
    LocalDateTime date = DateParser.parseToDateTime(requestDate);

    Pageable pageable = PageRequest.of(0, 1);
    List<PriceQueryProjectionDto> prices = priceRepository
        .findByBrandIdProductIdAndDate(1, 35455L, date, pageable);

    assertEquals(1, prices.get(0).getPriceList());
  }

}
