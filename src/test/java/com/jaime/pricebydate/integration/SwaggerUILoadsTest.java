package com.jaime.pricebydate.integration;

import static io.restassured.RestAssured.get;

import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("Spring Boot context load test")
@Slf4j
class SwaggerUILoadsTest {

  @LocalServerPort private int port;
  private String uri;

  @PostConstruct
  public void init() {
    uri = "http://localhost:" + port;
  }

  @Test
  @DisplayName("Testing swagger loads and is available via url")
  public void swaggerIsAvailableTest() {
    get(uri + "/swagger-ui/index.html/").then().statusCode(200);
  }
}
